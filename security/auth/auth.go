package auth

import (
	"errors"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/hashing"
)

func UserAuth(uid, token string) (bool, string, int, error) {

	if !ValidId(uid) || token == "" || uid == "" {
		log.Warn("User attempted to log in with incorrect token")
		return false, "", 400, errors.New("INVALID user id or token")
	}

	//Fetch hash of correct token from database
	rows, err := (&databases.PqDb).Query(`SELECT tokenhash, name FROM "users" WHERE id=$1;`, uid)
	if err != nil {
		return false, "", 500, errors.New("ERROR while retreaving data from db")
	}

	var tokenhash, name string
	var count = 0
	for rows.Next() {
		count++
		err = rows.Scan(&tokenhash, &name)
	}

	if count != 1 {
		return false, "", 400, errors.New("USER does not exist")
	}

	if err != nil {
		return false, "", 500, errors.New("DATABASE error")
	}

	//get salt from tokenhash
	fields := strings.Split(tokenhash, "$")
	salt := fields[4]

	//hash token for comparison with in-db data
	thash, err := hashing.ArgonHashSalted(token, salt)
	if err != nil {
		return false, "", 500, errors.New("HASHING error")
	}

	if thash == tokenhash {
		return true, name, 200, nil
	} else {
		return false, "", 401, errors.New("UNAUTHORIZED")
	}
}

func ValidId(id string) bool {
	//Check if id is base64 compliant
	regex := regexp.MustCompile(`[A-Za-z0-9]+`)

	return regex.MatchString(id)
}
