package hashing

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"golang.org/x/crypto/argon2"
)

type hashConfig struct {
	time    uint32
	memory  uint32
	threads uint8
	keyLen  uint32
}

var config = &hashConfig{
	time:    1,
	memory:  64 * 1024,
	threads: 4,
	keyLen:  32,
}

func ArgonHashSalted(pt, salt string) (string, error) {

	hash := argon2.IDKey([]byte(pt), []byte(salt), config.time, config.memory, config.threads, config.keyLen)

	// Base64 encode the hashed data.
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	format := "$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s"
	full := fmt.Sprintf(format, argon2.Version, config.memory, config.time, config.threads, salt, b64Hash)
	return full, nil

}

func NewArgonHashSalted(pt string) (string, error) {

	salt := make([]byte, 16)
	if _, err := rand.Read(salt); err != nil {
		return "", err
	}

	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	hash, err := ArgonHashSalted(pt, b64Salt)

	if err != nil {
		return "", err
	}

	return hash, nil

}
