package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/routes"
)

func main() {
	log.Info("Reading config...")
	config = readJSONFile("./config.json")
	log.Info("Connecting to primary database...")
	err := databases.PqInit(config["db_driver"], config["postgres_uri"])
	if err != nil {
		log.Error(err)
		log.Fatal("Could not connect to db")
	}
	err = databases.RedisInit(config["redis_uri"])
	if err != nil {
		log.Error(err)
		log.Fatal("Could not connect to redis")
	}
	//Router setup
	r := mux.NewRouter()
	r.HandleFunc("/", routes.Home).Methods("GET")
	r.HandleFunc("/login", routes.Login).Methods("GET")
	r.HandleFunc("/signup", routes.Signup).Methods("POST")
	r.HandleFunc("/getUser", routes.GetUser).Methods("GET")
	r.HandleFunc("/getInst", routes.GetInst).Methods("GET")
	r.HandleFunc("/createNode", routes.CreateNode).Methods("POST")
	r.HandleFunc("/createGroup", routes.CreateGroup).Methods("POST")
	r.HandleFunc("/deleteNode", routes.DeleteNode).Methods("DELETE")
	r.HandleFunc("/deleteGroup", routes.DeleteGroup).Methods("DELETE")
	r.HandleFunc("/createInstitution", routes.CreateInstitution).Methods("POST")
	http.Handle("/", &Server{r})
	log.Info("Running on ", config["serve"])
	err = http.ListenAndServe(config["serve"], nil)
	if err != nil {
		log.Error(err)
		log.Fatal("Could not start the web-server")
	}
}

//Stores data from config file (read at app start)
var config = make(map[string]string)

//Function to read data from a json file
func readJSONFile(file string) map[string]string {

	var data = make(map[string]string)

	//read config.json
	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	//Rarse contents of config.json
	if err := json.Unmarshal(content, &data); err != nil {
		log.Fatal(err)
	}

	return data
}

type Server struct {
	r *mux.Router
}

//globally set headers for respones using mux router
func (s *Server) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	rw.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Client-Id, Authorization")

	rw.Header().Set("Content-Type", "application/json")
	// Stop here if its Preflighted OPTIONS request
	if req.Method == "OPTIONS" {
		return
	}
	// Lets Gorilla work
	s.r.ServeHTTP(rw, req)
}
