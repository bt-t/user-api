package routes

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/auth"
	"gitlab.com/bt-t/user-api/security/hashing"
)

func CreateNode(w http.ResponseWriter, r *http.Request) {

	/* REQUIRED JSON BODY:
	 {
		"name":"",
		"group":""
	 }
	*/

	//Decode body
	decoder := json.NewDecoder(r.Body)
	var bdy struct {
		Name  string
		Group string
	}
	err := decoder.Decode(&bdy)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//sanitize name
	regex := regexp.MustCompile(`[A-Za-z0-9]+`)

	//Cheack that all required data is provided
	if bdy.Name == "" || len(bdy.Name) < 5 || len(bdy.Name) > 25 || !regex.MatchString(bdy.Name) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Name invalid\"}")
		return
	}

	if bdy.Group == "" || len(bdy.Group) != 30 || !regex.MatchString(bdy.Group) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Group id invalid\"}")
		return
	}

	//BEGIN AUTH
	userId := r.Header.Get("Client-Id")
	valid, _, statusCode, err := auth.UserAuth(userId, r.Header.Get("Authorization"))

	if !valid {
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, "{\"status\":\""+err.Error()+"\"}")
		return
	}
	//END AUTH

	//check that user is authorized to access the provided group
	rows, err := (&databases.PqDb).Query(`Select exists (select * from "groups" full join "institutions" on groups.institution=institutions.id where institutions."owner"=$1 and groups."id"=$2);`, userId, bdy.Group)
	instValid := false
	var count = 0
	for rows.Next() {
		count++
		err = rows.Scan(&instValid)
	}

	if err != nil || count != 1 {
		log.Error(err)
		internalServerError(w)
		return
	}

	if !instValid {
		w.WriteHeader(401)
		fmt.Fprintf(w, "{\"status\":\"UNAUTHORIZED\"}")
		return
	}

	//id is randomly genearted, then checked for duplicates and if duplicate regenerated
	var idGenerated = false
	var id string
	for !idGenerated {
		id, err = randomString(30)

		if err != nil {
			log.Error(err)
			internalServerError(w)
			return
		}

		rows, err := (&databases.PqDb).Query(`Select exists (select * from "nodes" where id=$1);`, id)
		idExists := false
		var count = 0
		for rows.Next() {
			count++
			err = rows.Scan(&idExists)
		}

		if err != nil || count != 1 {
			log.Error(err)
			internalServerError(w)
			return
		}

		if !idExists {
			idGenerated = true
		}
	}

	//randomly generate string, to be used as a token for auth
	nodeToken, err := randomString(50)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//hash token for db-storage
	nTkHash, err := hashing.NewArgonHashSalted(nodeToken)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//add node to the db
	_, err = (&databases.PqDb).Exec(`insert into "nodes" (name, id, tokenhash, groupid) values ($1, $2, $3, $4);`, bdy.Name, id, nTkHash, bdy.Group)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	type jRes struct {
		Token  string `json:"token"`
		Id     string `json:"id"`
		Group  string `json:"groupid"`
		Status string `json:"status"`
	}

	//send data to user
	b, err := json.Marshal(jRes{nodeToken, id, bdy.Group, "success"})

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("User ", userId, " created node ", id, " in group ", bdy.Group)
	w.WriteHeader(200)
	fmt.Fprint(w, string(b))

}
