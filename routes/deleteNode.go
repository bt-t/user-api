package routes

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/auth"
)

func DeleteNode(w http.ResponseWriter, r *http.Request) {
	/*
		Required body json:
		{"id":"<node's id>"}
	*/

	//Decode body
	decoder := json.NewDecoder(r.Body)
	var bdy struct {
		Id string `json:"id"`
	}
	err := decoder.Decode(&bdy)

	if err != nil {
		log.Error(err)
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Invalid body json\"}")
		return
	}

	//Regex for id validation
	regex := regexp.MustCompile(`[A-Za-z0-9]+`)

	if !regex.MatchString(bdy.Id) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Invalid id\"}")
		return
	}

	//BEGIN AUTH
	userId := r.Header.Get("Client-Id")
	valid, _, statusCode, err := auth.UserAuth(userId, r.Header.Get("Authorization"))

	if !valid {
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, "{\"status\":\""+err.Error()+"\"}")
		return
	}
	//END AUTH

	//Validate, that user actually owns the node
	rows, err := (&databases.PqDb).Query(`select u.id from "nodes" n
	inner join "groups" g  on n.groupid=g.id 
	inner join "institutions" i  on g.institution=i.id 
	inner join "users" u on i.owner=u.id 
	where n.id=$1;`, bdy.Id)

	var count int
	var uid string
	for rows.Next() {
		count++
		err = rows.Scan(&uid)
	}

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	if count == 0 {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Node does not exist\"}")
		return
	}

	if uid != userId {
		w.WriteHeader(401)
		fmt.Fprintf(w, "{\"status\":\"You are not authorized to delete that node\"}")
		return
	}

	//Delete node from db
	_, err = (&databases.PqDb).Exec(`delete from "nodes" where id = $1;`, bdy.Id)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("User ", userId, " deleted node ", bdy.Id)

	w.WriteHeader(200)
	fmt.Fprintf(w, "{\"status\":\"success\"}")

}
