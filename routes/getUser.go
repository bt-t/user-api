package routes

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/auth"
)

func GetUser(w http.ResponseWriter, r *http.Request) {
	//No body or parameters required, as all relevant data is present in headers

	//BEGIN AUTH
	userId := r.Header.Get("Client-Id")
	valid, username, statusCode, err := auth.UserAuth(userId, r.Header.Get("Authorization"))

	if !valid {
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, "{\"status\":\""+err.Error()+"\"}")
		return
	}
	//END AUTH

	type Institution struct {
		Name string `json:"name"`
		Id   string `json:"id"`
	}

	type Node struct {
		Name    string   `json:"name"`
		Id      string   `json:"id"`
		Group   string   `json:"group"`
		Devices []string `json:"devices"`
	}

	type Group struct {
		Name        string `json:"name"`
		Id          string `json:"id"`
		Institution string `json:"institution"`
	}

	//Fetch user's data
	rows, err := (&databases.PqDb).Query(`select i."name" instName, i.id instId, g."name" groupName, g.id groupId, n."name" nodeName, n.id nodeId
	from "users" u 
	FULL join "institutions" i on u.id=i.owner
	FULL join "groups" g on i.id=g.institution
	FULL join "nodes" n  on g.id=n.groupid 
	where u.id = $1`, userId)

	var insts []Institution
	var groups []Group
	var nodes []Node

	groupMap := make(map[Group]bool)
	instMap := make(map[Institution]bool)

	for rows.Next() {
		var instName, instId, groupName, groupId, nodeName, nodeId sql.NullString
		err = rows.Scan(&instName, &instId, &groupName, &groupId, &nodeName, &nodeId)

		//No duplicate check required,  as duplicates are impossible
		if nodeId.String != "" { //DONE
			nodes = append(nodes, Node{nodeName.String, nodeId.String, groupId.String, []string{}})
		}

		//Using a map to make time complexity closer to O(1) Boolean is useless, only present to make map work
		//Check if node key already exists, if so skip
		group := Group{groupName.String, groupId.String, instId.String}
		_, present := groupMap[group]
		if groupId.String != "" && !present {
			groups = append(groups, group)
			groupMap[group] = false
		}

		//Same as above, just for institutions
		inst := Institution{instName.String, instId.String}
		_, present = instMap[inst]
		if instId.String != "" && !present {
			insts = append(insts, inst)
			instMap[inst] = false
		}
	}

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//for each node get devices currently stored in db
	for i := 0; i < len(nodes); i++ {
		devices, err := databases.GetNodes([]string{"node_raw:" + nodes[i].Id})
		if err != nil {
			log.Error(err)
			internalServerError(w)
			return
		}
		nodes[i].Devices = devices
	}

	type response struct {
		Status       string        `json:"status"`
		Username     string        `json:"username"`
		Institutions []Institution `json:"institutions"`
		Groups       []Group       `json:"groups"`
		Nodes        []Node        `json:"nodes"`
	}

	res := response{
		Status:       "success",
		Username:     username,
		Institutions: insts,
		Groups:       groups,
		Nodes:        nodes}

	b, err := json.Marshal(res)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("Data of user ", userId, " served")

	w.WriteHeader(200)
	fmt.Fprint(w, string(b))

}
