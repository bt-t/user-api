package routes

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func Home(w http.ResponseWriter, r *http.Request) {
	log.Info("Root served")
	fmt.Fprintf(w, "CrowdControl user api up and running!")
}
