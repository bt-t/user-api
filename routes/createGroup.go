package routes

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/auth"
)

func CreateGroup(w http.ResponseWriter, r *http.Request) {

	/* REQUIRED JSON BODY:
	 {
		"name":"",
		"institution":""
	 }
	*/

	//Decode body
	decoder := json.NewDecoder(r.Body)
	var bdy struct {
		Name        string
		Institution string
	}
	err := decoder.Decode(&bdy)

	if err != nil {
		log.Error(err)
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Invalid body json\"}")
		return
	}

	//Regex for name and institution id validation
	regex := regexp.MustCompile(`[A-Za-z0-9]+`)

	//Check that all required data is present
	if bdy.Name == "" || bdy.Institution == "" || !regex.MatchString(bdy.Institution) || len(bdy.Name) < 5 || len(bdy.Name) > 25 || !regex.MatchString(bdy.Name) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Name invalid\"}")
		return
	}

	//BEGIN AUTH
	userId := r.Header.Get("Client-Id")

	valid, _, statusCode, err := auth.UserAuth(userId, r.Header.Get("Authorization"))

	if !valid {
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, "{\"status\":\""+err.Error()+"\"}")
		return
	}
	//END AUTH

	//check that institution exist & that user is owner of the provided inststutuion
	rows, err := (&databases.PqDb).Query(`Select exists (select * from "institutions" where owner=$1 AND id=$2);`, userId, bdy.Institution)
	instValid := false
	var count = 0
	for rows.Next() {
		count++
		err = rows.Scan(&instValid)
	}

	if err != nil || count != 1 {
		log.Error(err)
		internalServerError(w)
		return
	}

	if !instValid {
		w.WriteHeader(401)
		fmt.Fprintf(w, "{\"status\":\"UNAUTHORIZED\"}")
		return
	}

	//id is randomly genearted, then checked for duplicates and if duplicate regenerated
	var idGenerated = false
	var id string
	for !idGenerated {
		id, err = randomString(30)

		if err != nil {
			log.Error(err)
			internalServerError(w)
			return
		}

		rows, err := (&databases.PqDb).Query(`Select exists (select * from "groups" where id=$1);`, id)
		idExists := false
		var count = 0
		for rows.Next() {
			count++
			err = rows.Scan(&idExists)
		}

		if err != nil || count != 1 {
			log.Error(err)
			internalServerError(w)
			return
		}

		if !idExists {
			idGenerated = true
		}
	}

	//Write data to database name is pre-sanitized
	_, err = (&databases.PqDb).Exec(`INSERT INTO "groups" (name, id, institution) VALUES ($1, $2, $3);`, bdy.Name, id, bdy.Institution)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//send new area's id to the requesting user
	type resp struct {
		Status string `json:"status"`
		Id     string `json:"id"`
	}

	resmsg := &resp{Status: "success", Id: id}
	b, err := json.Marshal(resmsg)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("User ", userId, " created group ", id, " in institution ", bdy.Institution)
	w.WriteHeader(200)
	fmt.Fprint(w, string(b))
}
