package routes

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/auth"
)

func CreateInstitution(w http.ResponseWriter, r *http.Request) {

	/* REQUIRED JSON BODY:
	 {
		"name":""
	 }
	*/

	//Decode body json
	decoder := json.NewDecoder(r.Body)
	var bdy struct {
		Name string
	}
	err := decoder.Decode(&bdy)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//Regex for name sanitization(only 0-9, A-Z, a-z allowed)
	regex := regexp.MustCompile(`[A-Za-z0-9]+`)

	//Check that all parameters form body are valid
	if bdy.Name == "" || len(bdy.Name) < 5 || len(bdy.Name) > 25 || !regex.MatchString(bdy.Name) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Name invalid\"}")
		return
	}

	//BEGIN AUTH
	userId := r.Header.Get("Client-Id")
	valid, _, statusCode, err := auth.UserAuth(userId, r.Header.Get("Authorization"))

	if !valid {
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, "{\"status\":\""+err.Error()+"\"}")
		return
	}
	//END AUTH

	//Check that name is not duplicate
	rows, err := (&databases.PqDb).Query(`Select exists (select * from "institutions" where name=$1);`, bdy.Name)
	instExists := false
	var count = 0
	for rows.Next() {
		count++
		err = rows.Scan(&instExists)
	}

	if err != nil || count != 1 {
		log.Error(err)
		internalServerError(w)
		return
	}

	if instExists {
		w.WriteHeader(400)
		w.Write([]byte("{\"status\":\"Institutiona already exists\"}"))
		return
	}

	//id is md5 hash of name
	idSum := md5.Sum([]byte(bdy.Name))
	id := hex.EncodeToString(idSum[:])

	//Write data to database
	_, err = (&databases.PqDb).Exec(`INSERT INTO "institutions" (name, id, owner) VALUES ($1, $2, $3);`, bdy.Name, id, userId)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	type resp struct {
		Status string `json:"status"`
		Id     string `json:"id"`
	}

	resmsg := &resp{Status: "success", Id: id}
	b, err := json.Marshal(resmsg)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("User ", userId, " created institution ", id)
	fmt.Fprint(w, string(b))
}
