package routes

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
)

func GetInst(w http.ResponseWriter, r *http.Request) {

	vals := r.URL.Query()
	iid := vals["institutionId"]
	inme := vals["institutionName"]

	var id string
	var count = 0
	if len(iid) >= 1 {

		//sanitize id if it was provided
		regex := regexp.MustCompile(`[A-Za-z0-9]`)
		if !regex.MatchString(iid[0]) {
			w.WriteHeader(400)
			fmt.Fprintf(w, "{\"status\":\"Invalid id\"}")
			return
		}

		id = iid[0]

	} else if len(inme) >= 1 {
		//Fetch institution id if only a name was provided

		//sanitize name
		regex := regexp.MustCompile(`[A-Za-z0-9]+`)
		if !regex.MatchString(inme[0]) {
			w.WriteHeader(400)
			fmt.Fprintf(w, "{\"status\":\"Invalid username\"}")
			return
		}

		rows, err := (&databases.PqDb).Query(`select id from "institutions" i where i.name = $1`, inme[0])

		if err != nil {
			log.Error(err)
			internalServerError(w)
			return
		}

		for rows.Next() {
			err = rows.Scan(&id)
			count++
			if err != nil {
				log.Error(err)
				internalServerError(w)
				return
			}
		}

		if count < 1 {
			w.WriteHeader(400)
			fmt.Fprintf(w, "{\"status\":\"Institution does not exist\"}")
			return
		}

	} else {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Institution id and name missing in request\"}")
		return
	}

	//Fetch list of node ids based on the instId provided/pulled from db
	rows, err := (&databases.PqDb).Query(`select n.id nodeId, g.id groupId, g.name groupName
	from "nodes" n 
	full join "groups" g on n.groupid=g.id
	inner join "institutions" i on g.institution=i.id
	where i.id = $1`, id)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	type group struct {
		NodeIdArr []string
		Name      string
	}

	var groups = make(map[string]group)
	count = 0
	var nodeId sql.NullString
	var groupId sql.NullString
	var groupName sql.NullString
	for rows.Next() {
		count++
		err = rows.Scan(&nodeId, &groupId, &groupName)
		if err != nil {
			log.Error(err)
			internalServerError(w)
			return
		}

		//Add all nodes to their corresponding group, stored in a map for O(1)
		_, groupPresent := groups[groupId.String]
		if groupPresent {
			//modify group struct present in map
			curr := groups[groupId.String]
			curr.NodeIdArr = append(curr.NodeIdArr, "node_raw:"+nodeId.String)
			groups[groupId.String] = curr

		} else {
			//Create new group struct, add it ot map
			groups[groupId.String] = group{NodeIdArr: []string{"node_raw:" + nodeId.String}, Name: groupName.String}
		}
	}

	type data struct {
		Name    string `json:"name"`
		Devices int    `json:"devices"`
		Id      string `json:"id"`
	}
	//Iterate through map contents
	var groupVals []data
	for key, val := range groups {
		//fetch currently sensed bt addresses (within the current group) from db server
		devices, err := databases.GetNodes(val.NodeIdArr)
		if err != nil {
			log.Error(err)
			internalServerError(w)
			return
		}
		groupVals = append(groupVals, data{Name: groups[key].Name, Devices: len(devices), Id: key})
	}

	// Encode to json
	jsonString, err := json.Marshal(groupVals)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("Served data with institution id ", id)
	// Send data to user
	fmt.Fprint(w, string(jsonString))
}
