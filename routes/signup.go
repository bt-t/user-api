package routes

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/hashing"
)

type user struct {
	Email    string
	Username string
	Password string
}

func Signup(w http.ResponseWriter, r *http.Request) {

	/* REQUIRED JSON BODY:
	 {
		"email":"",
		"username":"",
		"password":""
	*/

	decoder := json.NewDecoder(r.Body)
	var u user
	err := decoder.Decode(&u)
	if err != nil ||
		u.Email == "" ||
		u.Password == "" ||
		u.Username == "" {

		w.WriteHeader(400)
		w.Write([]byte("{\"status\":\"Parameter missing\"}"))
		return
	}

	// Sanitize username (May only include cahrs a-z, A-Z, 0-9)
	regex := `[A-Za-z0-9]+`
	match, err := regexp.Match(regex, []byte(u.Username))
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	if !match || len(u.Username) < 5 || len(u.Username) > 25 || !CheckMail(u.Email) {
		w.WriteHeader(400)
		w.Write([]byte("{\"status\":\"Invalid username or email\"}"))
		return
	}

	//generate new hash with random salt and password
	hash, err := hashing.NewArgonHashSalted(u.Password)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	b64Mail := base64.RawStdEncoding.EncodeToString([]byte(u.Email))

	//Check if email already exists
	rows, err := (&databases.PqDb).Query(`Select exists (select * from "users" where email=$1);`, b64Mail)
	idExists := false
	var count = 0
	for rows.Next() {
		count++
		err = rows.Scan(&idExists)
	}

	if err != nil || count != 1 {
		log.Error(err)
		internalServerError(w)
		return
	}

	if idExists {
		w.WriteHeader(400)
		w.Write([]byte("{\"status\":\"User already exists\"}"))
		return
	}

	//id is randomly genearted, then checked for duplicates and if duplicate regenerated
	var idGenerated = false
	var id string
	for !idGenerated {
		id, err = randomString(30)

		if err != nil {
			internalServerError(w)
			return
		}

		rows, err := (&databases.PqDb).Query(`Select exists (select * from "users" where id=$1);`, id)
		idExists := false
		var count = 0
		for rows.Next() {
			count++
			err = rows.Scan(&idExists)
		}

		if err != nil || count != 1 {
			log.Error(err)
			internalServerError(w)
			return
		}

		if !idExists {
			idGenerated = true
		}
	}

	//Run Query on db (Username not b64 encoded since sanitization prevents characters that are not [A-Za-z0-9]+)
	_, err = (&databases.PqDb).Exec(`INSERT INTO "users" (name, id, passwordhash, email, emailvalid) VALUES ($1, $2, $3, $4, $5);`, u.Username, id, hash, b64Mail, false)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("Created user ", id)

	w.WriteHeader(200)
	w.Write([]byte("{\"status\":\"success\"}"))
}

func CheckMail(mail string) bool {
	pattern := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return pattern.MatchString(mail)
}
