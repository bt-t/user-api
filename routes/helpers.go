package routes

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"net/http"

	log "github.com/sirupsen/logrus"
)

//secure random string generator
func randomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	ret := make([]byte, n)
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			log.Error(err)
			return "", err
		}
		ret[i] = letters[num.Int64()]
	}

	return string(ret), nil
}

func internalServerError(w http.ResponseWriter) {
	w.WriteHeader(500)
	fmt.Fprintf(w, "{\"status\":\"Internal server error\"}")
}
