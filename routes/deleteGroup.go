package routes

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/auth"
)

func DeleteGroup(w http.ResponseWriter, r *http.Request) {
	/*
		Required body json:
		{"id":"<group's id>"}
	*/

	//Decode body
	decoder := json.NewDecoder(r.Body)
	var bdy struct {
		Id string `json:"id"`
	}
	err := decoder.Decode(&bdy)

	if err != nil {
		log.Error(err)
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Invalid body json\"}")
		return
	}

	//Regex for id validation
	regex := regexp.MustCompile(`[A-Za-z0-9]+`)

	if !regex.MatchString(bdy.Id) {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Invalid id\"}")
		return
	}

	//BEGIN AUTH
	userId := r.Header.Get("Client-Id")
	valid, _, statusCode, err := auth.UserAuth(userId, r.Header.Get("Authorization"))

	if !valid {
		w.WriteHeader(statusCode)
		fmt.Fprintf(w, "{\"status\":\""+err.Error()+"\"}")
		return
	}
	//END AUTH

	//Validate, that user actually owns the group
	rows, err := (&databases.PqDb).Query(`select u.id from "groups" g
	inner join "institutions" i  on g.institution=i.id 
	inner join "users" u on i.owner=u.id 
	where g.id=$1;`, bdy.Id)

	var count int = 0
	var uid string = ""
	for rows.Next() {
		count++
		err = rows.Scan(&uid)
	}

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	if count == 0 {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Group does not exist\"}")
		return
	}

	if uid != userId {
		w.WriteHeader(401)
		fmt.Fprintf(w, "{\"status\":\"You are not authorized to delete that group\"}")
		return
	}

	//Delete group's nodes from db
	_, err = (&databases.PqDb).Exec(`delete from "nodes" where groupid = $1;`, bdy.Id)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//Delete group itself (done seperatly, as pq can not have multiuple sql querries in one prepared statement)
	_, err = (&databases.PqDb).Exec(`delete from "groups" where id = $1;`, bdy.Id)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("User ", userId, " deleted group ", bdy.Id)

	w.WriteHeader(200)
	fmt.Fprintf(w, "{\"status\":\"success\"}")

}
