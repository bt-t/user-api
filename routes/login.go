package routes

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/user-api/databases"
	"gitlab.com/bt-t/user-api/security/hashing"
)

func Login(w http.ResponseWriter, r *http.Request) {
	vals := r.URL.Query()
	password := vals["password"]
	mail := vals["email"]

	var passwd = ""
	var email = ""
	if len(password) >= 1 && len(mail) >= 1 {
		passwd = password[0]
		email = mail[0]
	} else {
		w.WriteHeader(400)
		fmt.Fprintf(w, "{\"status\":\"Email or password field missing\"}")
		return
	}

	validLogin, uid, err := CheckUserCreds(email, passwd)

	if !validLogin {
		w.WriteHeader(401)
		fmt.Fprintf(w, "{\"status\":\"Invalid login\"}")
		return
	}

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//Generate random token
	rstring, err := randomString(50)
	token := "Bearer " + rstring

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//hash token for db storage
	thash, err := hashing.NewArgonHashSalted(token)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//Store token in db
	_, err = (&databases.PqDb).Exec(`UPDATE "users" set tokenhash = $1 WHERE id=$2;`, thash, uid)
	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	//send unhashed token to user
	type resp struct {
		Status string `json:"status"`
		Id     string `json:"id"`
		Token  string `json:"token"`
	}

	resmsg := &resp{Status: "success", Token: token, Id: uid}
	b, err := json.Marshal(resmsg)

	if err != nil {
		log.Error(err)
		internalServerError(w)
		return
	}

	log.Info("User ", uid, " successfully logged in")
	fmt.Fprint(w, string(b))

}

func CheckUserCreds(email, password string) (bool, string, error) {

	b64Mail := base64.RawStdEncoding.EncodeToString([]byte(email))

	//fetch user by email
	rows, err := (&databases.PqDb).Query(`SELECT passwordhash, id FROM "users" WHERE email=$1;`, b64Mail)
	if err != nil {
		log.Error(err)
		return false, "", errors.New("ERROR while retreaving data from db")
	}

	var passwordhash string
	var id string
	var count = 0
	for rows.Next() {
		count++
		err = rows.Scan(&passwordhash, &id)
	}

	if count != 1 {
		return false, "", errors.New("USER does not exist")
	}

	if err != nil {
		log.Error(err)
		return false, "", err
	}

	//Decode argon2 string
	fields := strings.Split(passwordhash, "$")
	salt := fields[4]

	//password passed into function
	newhash, err := hashing.ArgonHashSalted(password, salt)
	if err != nil {
		log.Error(err)
		return false, "", errors.New("HASHING unsuccessful")
	}

	//Return
	return passwordhash == newhash, id, nil
}
