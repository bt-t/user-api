create table users(
  name varchar(25) NULL,
  id varchar(32) NULL PRIMARY KEY,
  tokenhash varchar(100) null,
  passwordhash varchar(100) null,
  email varchar(100) null,
  emailValid boolean
);

CREATE TABLE institutions (
  id VARCHAR(32) NULL PRIMARY KEY,
  owner varchar(32) null,
  name VARCHAR(25) NULL,
  foreign key (owner) references users(id)
);

CREATE TABLE groups (
  name VARCHAR(25) NULL,
  id VARCHAR(32) NULL PRIMARY KEY,
  institution VARCHAR(32) NULL,
  FOREIGN KEY (institution) REFERENCES institutions(id)
);

CREATE TABLE nodes (
  name VARCHAR(25) NULL,
  id VARCHAR(32) NULL PRIMARY KEY,
  tokenhash VARCHAR(100) NULL,
  groupid VARCHAR(32) NULL,
  FOREIGN KEY (groupid) REFERENCES groups(id)
);