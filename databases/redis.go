package databases

import (
	"context"

	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

var RedisCtx = context.Background()

//Empty db connection, is used in redisInit()
var Rdb *redis.Client

func RedisInit(uri string) error {

	log.Info("Connectiong to redis-db...")

	opt, err := redis.ParseURL(uri)
	if err != nil {
		return err
	}

	Rdb = redis.NewClient(opt)

	log.Info("Testing connection...")

	err = Rdb.Set(RedisCtx, "test", "test", 0).Err()
	if err != nil {
		return err
	}

	_, err = Rdb.Get(RedisCtx, "test").Result()
	if err != nil {
		return err
	}

	log.Info("Connected!")

	return nil

}

//Uses sunions to fetch multiple keys at once, no duplicate values possible
func GetNodes(keys []string) ([]string, error) {
	return Rdb.SUnion(RedisCtx, keys...).Result()
}
